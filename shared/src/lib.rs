#[derive(Debug)]
pub struct SharedStruct {
    pub name: String,
    pub age: u8,
}

pub fn shared_function() {
    println!("Hello from shared_function");
}
