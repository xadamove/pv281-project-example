fn foo_private() {
    // This function is private to this module.
    print!("Hello from foo_private");
}

pub fn foo_public() {
    print!("Hello from foo_public");

    // `bar` can call `foo_private` because they're both in the same module.
    foo_private();
}
