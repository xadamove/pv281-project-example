pub fn bar() {
    println!("Hello from sub_one::bar");
}

pub fn call_other_stuff() {
    // These are all equivalent.
    bar();
    self::bar(); // `self` refers to the current module.
    crate::helpers::bar::bar(); // `crate` refers to the root module of the crate (main.rs).
    super::bar::bar(); // `super` refers to the parent module (foo.rs).
    super::super::helpers::bar::bar(); // `super` can be chained.

    // You can also call functions from other modules if they are visible
    crate::foo::foo_public();
}
