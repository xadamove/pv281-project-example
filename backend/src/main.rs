// Top-level module (src/foo.rs)
mod foo;

// Subdirectories can be used to organize modules (src/sub_one)
mod helpers {
    // Nested module has to be declared public to be visible outside of `mod helpers`.
    pub mod bar; // (src/sub_one/bar.rs)
}

fn main() {
    foo::foo_public();
    // foo::foo_private(); // Err: `foo_private` is private by default.

    // Call `bar` from `helpers` module.
    helpers::bar::call_other_stuff();

    // We can call functionality and use types from the shared library between BE and FE (../../shared/src/lib.rs)
    // See how it is declared in Cargo.toml
    shared::shared_function();

    let model = shared::SharedStruct {
        name: "John".to_string(),
        age: 42,
    };

    println!("{model:?}")
}
