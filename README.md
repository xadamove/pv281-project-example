# Large project example

This repository showcases an example of how you can structure more complex codebase in Rust for you to use as a reference when you start your own project. Keep in mind that there is no single correct way to structure your code, and you should always consider your own needs and preferences.

## Repository structure

The repository contains three packages:

- `backend`: a binary, provides an introduction to organizing code into modules using the `mod` keyword and the effects of `pub` keyword (visibility)
- `frontend`: a binary
- `shared`: a library used by both `backend` and `frontend`

## Running multiple packages

If we run `cargo run` at the top level, we get a sensible error message:

```console
error: `cargo run` could not determine which binary to run. Use the `--bin` option to specify a binary, or the `default-run` manifest key.
available binaries: backend, frontend
```

We can run the backend with `cargo run --bin backend` and the frontend with `cargo run --bin frontend`.

> `cargo run` from `./backend` or `./frontend` will also work and run the binary in the current directory.
